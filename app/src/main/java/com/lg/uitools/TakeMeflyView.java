package com.lg.uitools;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * create by liguo on 19-5-8
 */
public class TakeMeflyView extends View {
    private static final String TAG = TakeMeflyView.class.getSimpleName();
    /**
     * 初始--静止
     */
    private final int DOG_STATE_DEFAULT = 0;
    /**
     * 范围内
     */
    private final int DOG_STATE_MOVE = 1;

    int DOG_STATE=DOG_STATE_DEFAULT;

    Paint mPaint;
    PointF mPaintCenter;
    float mDogRadius;


    Paint mEarthPaint;
    PointF mEarthPaintCenter;
    float mEarthRadius;

    //最大范围
    private float mMaxDist;
    /**
     * 当前距离
     */
    private float mDist;

    /**
     * 手指触摸偏移量
     */
    private final float MOVE_OFFSET;

    public TakeMeflyView(Context context) {
        this(context,null);
    }

    public TakeMeflyView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public TakeMeflyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TakeMeflyView, defStyleAttr, 0);
        mDogRadius = array.getDimension(R.styleable.TakeMeflyView_dog_radius, mDogRadius);
        array.recycle();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.FILL);
        mMaxDist = 8 * mDogRadius;
        MOVE_OFFSET= mMaxDist /4;
        mEarthRadius=mMaxDist;
        mEarthPaint= new Paint(Paint.ANTI_ALIAS_FLAG);
        mEarthPaint.setColor(Color.BLUE);
        mEarthPaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //圆心
        if (mEarthPaintCenter == null){
            mEarthPaintCenter = new PointF(w/2,h/2);
        }else{
            mEarthPaintCenter.set(w/2,h/2);
        }
        //圆心
        if (mPaintCenter == null){
            mPaintCenter = new PointF(w/2,h/2);
        }else{
            mPaintCenter.set(w/2,h/2);
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(mEarthPaintCenter.x, mEarthPaintCenter.y, mEarthRadius, mEarthPaint);
        Log.d(TAG,"DOG_STATE="+DOG_STATE);
        if (DOG_STATE == DOG_STATE_DEFAULT){//当拖拽的距离在指定范围内，那么调整不动气泡的半径
            canvas.drawCircle(mEarthPaintCenter.x, mEarthPaintCenter.y, mDogRadius, mPaint);

        }else {
            canvas.drawCircle(mPaintCenter.x, mPaintCenter.y, mDogRadius, mPaint);

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch ( event.getAction()){
            case MotionEvent.ACTION_DOWN:
                    DOG_STATE = DOG_STATE_MOVE;


                break;
            case MotionEvent.ACTION_MOVE:
                if (DOG_STATE != DOG_STATE_DEFAULT){
                    mDist = (float) Math.hypot(event.getX() - mEarthPaintCenter.x, event.getY()-mEarthPaintCenter.y);
                    mPaintCenter.x = event.getX();
                    mPaintCenter.y = event.getY();
                    Log.d(TAG,"mDist="+mDist);
                    if (mDist > mMaxDist ) {
                        DOG_STATE = DOG_STATE_DEFAULT;
                    }else{
                        DOG_STATE =DOG_STATE_MOVE;
                    }
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                DOG_STATE = DOG_STATE_DEFAULT;
                invalidate();
                break;
        }

        return true;
    }
}
